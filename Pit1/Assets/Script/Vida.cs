﻿using UnityEngine;
using System.Collections;

public class Vida : MonoBehaviour {
	// Total hitpoints
	public int hp = 3;
    public GameObject rip;
    public GameObject personaje;

    public AudioClip itemsoundImpact;
    public float itemsoundvolumeImpact = 0.5f;
    public AudioClip itemsoundEnemyDead;
    public AudioClip itemsoundPersonaje;


    Animator anim;
   

       void Start()
    {
      
        anim = this.gameObject.GetComponent<Animator>();  
    }

     void Update()
    {
        rip.transform.position = personaje.transform.position;
    }
    /// Inflicts damage and check if the object should be destroyed
    public void Damage() { //controlar y matar personaje
		hp--;
        
		if (hp <= 0){
            // Dead!
            anim.enabled = false;
            
            this.gameObject.GetComponent<Renderer>().material.color = Color.red;
            AudioSource.PlayClipAtPoint(itemsoundEnemyDead, Camera.main.transform.position, itemsoundvolumeImpact);
            ;



            Destroy(gameObject ,1.5f);
        }
	}


    void OnTriggerEnter2D(Collider2D other) {

        Debug.Log("entro");

        if (other.tag == "shot" && tag == "Enemy") {//esto para destruir el enemigo
            AudioSource.PlayClipAtPoint(itemsoundImpact, Camera.main.transform.position, itemsoundvolumeImpact);
            Damage();
           
            Destroy (other.gameObject);
		}

		if (other.tag == "Player" && this.gameObject.GetComponent<Renderer>().material.color != Color.red) {
            Debug.Log("muerto");
            if (personaje != null) {
                AudioSource.PlayClipAtPoint(itemsoundPersonaje, Camera.main.transform.position, itemsoundvolumeImpact);
                rip.SetActive(true);
                Destroy(personaje);
            }
            
           
		}

		if (other.tag == "Muerte") {
			Destroy (gameObject);
		}
	}
}