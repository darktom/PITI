﻿using UnityEngine;
using System.Collections;

public class Bala : MonoBehaviour {

	public GameObject personaje; // Para saber la direccion

	public Rigidbody2D projectile;
	public float speed = 1f;


	// Use this for initialization
	void Start (){
		
	}
	
	// Update is called once per frame
	void Update (){
		if (Input.GetButtonDown("Fire1")) {
			float direccion = personaje.transform.localScale.x;
			Rigidbody2D instantiatedProjectile = (Rigidbody2D)Instantiate (projectile, transform.position ,transform.rotation);

			if(direccion < 0)
				instantiatedProjectile.velocity = transform.TransformDirection(new Vector2 (-speed,0));
			else
				instantiatedProjectile.velocity = transform.TransformDirection(new Vector2 (speed,0));



		}
	}
}