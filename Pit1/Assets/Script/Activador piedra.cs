﻿using UnityEngine;
using System.Collections;

public class Activadorpiedra : MonoBehaviour {
    public GameObject piedra; 

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            piedra.SetActive(true);
        }
    }
}
