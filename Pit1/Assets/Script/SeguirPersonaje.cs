﻿using UnityEngine;
using System.Collections;

public class SeguirPersonaje : MonoBehaviour {

	public Transform personaje;
    public Transform Esquela;
    public GameObject rip;

	
	
	// Update is called once per frame
	void Update () {
        if (personaje != null)
        {
            Debug.Log("la posicion que llevas: " + transform.position);
            transform.position = new Vector3(personaje.position.x, transform.position.y, transform.position.z);
        }
        else
        {
            Esquela.position = transform.position;
            rip.SetActive(true);
            
        }
	}
}
