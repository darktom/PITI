using System;
using UnityEngine;


namespace UnityStandardAssets.Utility
{
    public class FollowTarget : MonoBehaviour
    {
        public Transform target;
        public Vector3 offset = new Vector3(0f, 7.5f, 0f);
        public GameObject personaje;
        public GameObject rip;

        private void LateUpdate()
        {
            transform.position = target.position + offset;
        }
        void update()
        {

        if(!personaje){
                rip.SetActive(true);

            }
        }
    }
}
