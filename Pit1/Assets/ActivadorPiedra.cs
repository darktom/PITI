﻿using UnityEngine;
using System.Collections;

public class ActivadorPiedra : MonoBehaviour
{
    public GameObject Bloque;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("he entrado");
            Destroy(Bloque, 2f);
        }
    }
}
